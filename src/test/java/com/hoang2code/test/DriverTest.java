package com.hoang2code.test;

import com.java8.predicate.Rules;
import com.java8.supplier.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class DriverTest {

    private WebDriver driver;

    @BeforeTest
    @Parameters("browser")
    public void setDriver(@Optional("chrome") String browser) {
        this.driver = DriverFactory.gerDriver("firefox");
    }

    @Test
    public void googleTest() {
        this.driver.get("https://google.com");
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        this.driver.manage().window().maximize();
        List<WebElement> elements = this.driver.findElements(By.tagName("a"));

        Predicate<WebElement> isBlankLink = (ele) -> ele.getText().trim().isEmpty();
        Predicate<WebElement> isSContained = (ele) -> ele.getText().toLowerCase().contains("s");

        elements.removeIf(isBlankLink.or(isSContained));
        elements.forEach(element -> System.out.println(element.getText()));

        Rules.get().forEach(rule -> elements.removeIf(rule));


        // remove link which contains "s"

       /* for (WebElement element : elements) {
            if (!isBlankLink.test(element))
                System.out.println(element.getText());

        }*/





    }

    @AfterTest
    public void tearDown() {
        this.driver.quit();
    }


}
