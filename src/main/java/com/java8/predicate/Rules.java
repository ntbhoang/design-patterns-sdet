package com.java8.predicate;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Rules {


    private static Predicate<WebElement> isBlankLink = (ele) -> ele.getText().trim().isEmpty();
    private static Predicate<WebElement> isSContained = (ele) -> ele.getText().toLowerCase().contains("s");


    public static List<Predicate<WebElement>> get() {
        List<Predicate<WebElement>> list = new ArrayList<>();
        list.add(isBlankLink);
        list.add(isSContained);

        return list;
    }
}
