package com.java8.assigment;

@FunctionalInterface
public interface MathOperation {
    int operate(int a, int b);
}
