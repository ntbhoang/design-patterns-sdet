package com.java8.function;

import java.util.function.Function;

public class FunctionMain {
    public static void main(String[] args) {
        Function<Integer, Integer> plus2 = (i) -> i + 2; // input is a string, output is an integer
        Function<Integer, Integer> square = (i) -> i * i;


        System.out.println(

                plus2.andThen(square).apply(5) // (5 + 2) * (5 +2)
        );

        System.out.println(
                plus2.compose(square).apply(5) // (5 * 5) + 2
        );


    }
}
