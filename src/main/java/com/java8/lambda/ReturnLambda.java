package com.java8.lambda;

@FunctionalInterface
public interface ReturnLambda {
    int sum(int a, int b);
}
