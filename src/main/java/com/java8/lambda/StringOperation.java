package com.java8.lambda;

@FunctionalInterface
public interface StringOperation {
    String accept(String s1, String s2);
}
