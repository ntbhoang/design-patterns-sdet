package com.java8.lambda;

// SAM : single Abstract method

@FunctionalInterface
public interface GreetingService {
     String greet(String name);
}
