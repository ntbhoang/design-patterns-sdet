package com.java8;

import com.java8.assigment.MathOperation;
import com.java8.lambda.GreetingService;

public class Main {

    public static void main(String[] args) {

        MathOperation add = (a, b) -> a + b;
        MathOperation subtract = (a, b) -> a - b;
        MathOperation multiply = (a, b) -> a * b;
        MathOperation divide = (a, b) -> a / b;


        /*===================*/

        int onScreenNumber = 0;
        onScreenNumber = calculateRefactor(5, add, 2);
        onScreenNumber = calculateRefactor(onScreenNumber, subtract, 3);
        onScreenNumber = calculateRefactor(onScreenNumber, multiply, 7);
        onScreenNumber = calculateRefactor(onScreenNumber, add, 2);
        onScreenNumber = calculateRefactor(onScreenNumber, divide, 3);


        /*==========:5+2-3*7+2/3:*/

        String sequence = "5+2-3*7+2/3";
        int indexChar = sequence.indexOf("+");


        System.out.println(onScreenNumber);


    }

    private static int calculate(MathOperation mathOperation) {
        int a = 5;
        int b = 10;
        return mathOperation.operate(a, b);
    }

    private static int calculateRefactor(int onScreenNumber, MathOperation mathOperation, int enteredNumber) {
        return mathOperation.operate(onScreenNumber, enteredNumber);
    }


}
