package com.java8.consumer;

import java.util.function.Consumer;

public class ConsumerMain {

    public static void main(String[] args) {
        Consumer<String> dbConsumer = (s) -> {
            System.out.println("I am writing into DB ::  " + s);
        };

        Consumer<String> loggingConsumer = (s) -> {
            System.out.println("I am writing into log file ::  " + s);
        };

        Consumer<String> dbLogConsumer = dbConsumer.andThen(loggingConsumer);


    }
}
